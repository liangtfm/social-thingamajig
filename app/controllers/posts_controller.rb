class PostsController < ApplicationController

  def new
    @post = Post.new
    3.times { @post.links.build }

    @friend_circles = current_user.friend_circles

    render :new
  end

  def create
    @post = Post.new(params[:post])
    @post.user_id = current_user.id
    link_attributes = params[:link_attributes].values.reject { |link| link["url"].empty? }
    @post.links.new(link_attributes)

    if @post.save
      redirect_to post_url(@post)
    else
      flash[:errors] = @post.errors.full_messages
      redirect_to new_post_url
    end
  end

  def show
    @post = Post.includes(:links).find(params[:id])
    render :show
  end

  def index

  end

end
