class SessionsController < ApplicationController

  def create
    @user = User.find_by_credentials(params[:user][:email], params[:user][:password])
    if @user
      self.login(@user)
      redirect_to user_url(@user)
    else
      flash[:errors] = ["invalid email or password"]
      redirect_to new_session_url
    end
  end

  def new
    @user = User.new
    render :new
  end

  def destroy
    @user = self.current_user
    @user.reset_session_token!
    session[:session_token] = nil
    redirect_to new_session_url
  end

end
