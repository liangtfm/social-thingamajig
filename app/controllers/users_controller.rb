class UsersController < ApplicationController

  def create
    @user = User.new(params[:user])
    @user.posts.new(params[:post])
    @post = @user.posts.first
    link_attributes = params[:link_attributes].values.reject { |link| link["url"].empty? }
    @post.links.new(link_attributes)

    if @user.save
      self.login(@user)
      redirect_to user_url(@user)
    else
      flash[:errors] = @user.errors.full_messages
      redirect_to new_user_url
    end
  end

  def new
    @user = User.new
    @post = Post.new
    3.times { @post.links.build }

    render :new
  end

  def show
    @user = User.find(params[:id])
  end

  def feed
    @user = User.find(params[:user_id])
    @feed_posts = @user.feed_posts
  end

  def forgot_password
    render :enter_email
  end

  def generate_reset_password
    @user = User.find_by_email(params[:user][:email])
    random_password = SecureRandom.urlsafe_base64
    @user.password = random_password
    @user.save!
    msg = UserMailer.password_reset_email(@user, random_password)
    msg.deliver!
    render :check_email
  end

  def reset_password
    @user = User.find(params[:user_id])

    if @user.is_password?(params[:password])
      render :edit
    else
      flash[:errors] = ["Failed password reset! Please contact us."]
      redirect_to new_session_url
    end
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(params[:user])
      self.login(@user)
      redirect_to user_url(@user)
    else
      flash[:errors] = @user.errors.full_messages
      redirect_to user_reset_password_url(@user.id)
    end
  end

end
