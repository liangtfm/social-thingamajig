class FriendCirclesController < ApplicationController

  def new
    @friend_circle = FriendCircle.new
    @users = User.all

    render :new
  end

  def create
    @friend_circle = FriendCircle.new(params[:friend_circle])
    @friend_circle.owner_id = current_user.id

    if @friend_circle.save
      redirect_to friend_circle_url(@friend_circle)
    else
      flash[:errors] = @friend_circle.errors.full_messages
      redirect_to new_friend_circle_url
    end
  end

  def index

  end

  def show
    @friend_circle = FriendCircle.includes(:members).find(params[:id])

    render :show
  end

  def edit
    @friend_circle = FriendCircle.includes(:members).find(params[:id])
    @users = User.all

    render :edit
  end

  def update
    @friend_circle = FriendCircle.find(params[:id])
    if @friend_circle.update_attributes(params[:friend_circle])
      redirect_to friend_circle_url(@friend_circle)
    else
      flash[:errors] = @friend_circle.errors.full_messages
      redirect_to edit_friend_circle_url(@friend_circle)
    end
  end

  def destroy

  end
end
