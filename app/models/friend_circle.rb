# == Schema Information
#
# Table name: friend_circles
#
#  id         :integer          not null, primary key
#  owner_id   :integer          not null
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FriendCircle < ActiveRecord::Base
  attr_accessible :owner_id, :name, :member_ids

  validates :owner_id, presence: true

  has_many :memberships,
           :foreign_key => :friend_circle_id,
           :primary_key => :id,
           :class_name => "FriendCircleMembership",
           :inverse_of => :friend_circle

  has_many :members,
           :through => :memberships,
           :source => :member

  belongs_to :owner,
             :foreign_key => :owner_id,
             :primary_key => :id,
             :class_name => "User"

  has_many :post_shares,
           :foreign_key => :friend_circle_id,
           :primary_key => :id,
           :class_name => "PostShare"
end
