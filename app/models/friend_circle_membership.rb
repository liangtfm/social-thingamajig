# == Schema Information
#
# Table name: friend_circle_memberships
#
#  id               :integer          not null, primary key
#  member_id        :integer
#  friend_circle_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class FriendCircleMembership < ActiveRecord::Base
  attr_accessible :member_id, :friend_circle_id

  validates :member_id, :friend_circle, presence: true

  belongs_to :member,
             :foreign_key => :member_id,
             :primary_key => :id,
             :class_name => "User"

  belongs_to :friend_circle,
             :foreign_key => :friend_circle_id,
             :primary_key => :id,
             :class_name => "FriendCircle"

end
