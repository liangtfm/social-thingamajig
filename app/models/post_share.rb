# == Schema Information
#
# Table name: post_shares
#
#  id               :integer          not null, primary key
#  post_id          :integer          not null
#  friend_circle_id :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PostShare < ActiveRecord::Base
  attr_accessible :post_id, :friend_circle_id

  validates :post, :friend_circle_id, presence: true

  belongs_to :post,
  :foreign_key => :post_id,
  :primary_key => :id,
  :class_name => "Post"

  belongs_to :friend_circle,
  :foreign_key => :friend_circle_id,
  :primary_key => :id,
  :class_name => "FriendCircle"

end
