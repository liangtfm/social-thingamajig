# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)      not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ActiveRecord::Base
  attr_accessible :email, :password
  attr_reader :password

  before_validation(:ensure_session_token)

  validates :password, length: { minimum: 6, allow_nil: true }
  validates :email, :password_digest, :session_token, presence: true
  validates :email, uniqueness: true

  has_many :friend_circles,
           :foreign_key => :owner_id,
           :primary_key => :id,
           :class_name => "FriendCircle"

  has_many :friend_circle_memberships,
           :foreign_key => :member_id,
           :primary_key => :id,
           :class_name => "FriendCircleMembership"

  has_many :friends,
           :through => :friend_circles,
           :source => :members

  has_many :posts,
           :foreign_key => :user_id,
           :primary_key => :id,
           :class_name => "Post",
           :inverse_of => :author

  def feed_posts

    Post.joins(:post_shares).joins("JOIN friend_circle_memberships ON post_shares.friend_circle_id = friend_circle_memberships.friend_circle_id")
        .joins("JOIN users ON friend_circle_memberships.member_id = users.id")
        .where("users.id = ?", self.id).includes(:links).includes(:author)

  end

  def self.find_by_credentials(email, password)
    @user = User.find_by_email(email)

    (@user && @user.is_password?(password)) ? @user : nil

  end

  def password=(secret)
    @password = secret
    self.password_digest = BCrypt::Password.create(secret)
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end

  def ensure_session_token
    self.session_token ||= SecureRandom.urlsafe_base64
  end

  def reset_session_token!
    self.session_token = SecureRandom.urlsafe_base64
    self.save!
  end


end
