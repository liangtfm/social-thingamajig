# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  body       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Post < ActiveRecord::Base
  attr_accessible :user_id, :body, :friend_circle_ids

  validates :author, :body, presence: true

  belongs_to :author,
  :foreign_key => :user_id,
  :primary_key => :id,
  :class_name => "User"

  has_many :post_shares,
  :foreign_key => :post_id,
  :primary_key => :id,
  :class_name => "PostShare",
  :inverse_of => :post

  has_many :friend_circles,
  :through => :post_shares,
  :source => :friend_circle

  has_many :links,
  :foreign_key => :post_id,
  :primary_key => :id,
  :class_name => "Link",
  :inverse_of => :post
end
