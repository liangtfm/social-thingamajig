class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def password_reset_email(user, random_password)
    @user = user
    @url = "#{user_reset_password_url(@user.id)}?password=#{random_password}"

    mail(to: user.email, subject: "Password Reset Link")
  end
end
