SocialThingamajig::Application.routes.draw do

  resources :users do
    get 'reset_password'
    get 'feed'
  end
  resource :session
  get 'forgot_password', controller: :users
  post 'generate_reset_password', controller: :users

  resources :friend_circles
  resources :posts

end
